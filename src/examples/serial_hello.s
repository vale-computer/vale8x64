; Copyright (C) 2018-2019 Francis Mahnke.

; This file is part of VALE8x64.

; VALE8x64 is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.

; VALE8x64 is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.

; You should have received a copy of the GNU General Public License
; along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.

jmp x00,start

.include "io.s"

start:

li x34,$0200

call x00,io_print

;Forever
li x2,$05
bne x1,x2,$00,$00

.org $200
.byte $0d,$0a,"VALE8x64 64Kib RAM"
.byte $0d,$0a,$0a,"Ready."
.byte $0d,$0a,$0a,$00
.byte "moretexthere",$00
