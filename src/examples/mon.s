; Copyright (C) 2018-2019 Francis Mahnke.

; This file is part of VALE8x64.

; VALE8x64 is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.

; VALE8x64 is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.

; You should have received a copy of the GNU General Public License
; along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.

jmp x00,start

; Monitor entry
.org $0010
jmp x00,mon_start
.org $0013
ret

data: .org $0200
enter_mon: .byte $0d,$0a,"Enter monitor",$00
return_mon: .byte $0d,$0a,"Returned from monitor",$00
end_prog: .byte $0d,$0a,"End program",$00

start:
  li x34,enter_mon
  call x00,io_print

  li x1,$c3
  sub sp,x0,$01
  sb x1,$00(sp)

  li x12,$1234
  li x34,$5678
  li x56,$9abc
  li x78,$def0

  brk

  li x34,return_mon
  call x00,io_print

  li x34,end_prog
  call x00,io_print

  li x1,$01
  bne x1,x0,$00,$0000

.include "io.s"
.include "mon.s"
