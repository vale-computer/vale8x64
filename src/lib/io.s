; Copyright (C) 2018-2019 Francis Mahnke.

; This file is part of VALE8x64.

; VALE8x64 is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.

; VALE8x64 is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.

; You should have received a copy of the GNU General Public License
; along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.

.define SER_RX $00f1
.define SER_IS_BUSY $00f2
.define SER_TX $00f3

;
; io_print: print a NUL-terminated
;   string to the serial port
;
; inputs
;   x34: address of first character of
;     string
;
io_print:
  ; Load next char.
  - lb x1,$00(x34)

  ; Bail if char is zero.
  beq x1,x0,$00,++

  ; Transfer char
  mov x2,x1,$00

  ; Push address of char.
  sub sp,x0,$02
  mov x1,x3,$00
  sb x1,$01(sp)
  mov x1,x4,$00
  sb x1,$00(sp)

  ; Wait until tx free
  li x34,SER_IS_BUSY
  lb x1,$00(x34)
  bne x1,x0,$00,$fe

  ; Output next char
  li x34,SER_TX
  sb x2,$00(x34)

  ; Pull address of char.
  lb x1,$01(sp)
  mov x3,x1,$00
  lb x1,$00(sp)
  mov x4,x1,$00
  add sp,x0,$02

  ; Increment address of character
  add x34,x0,$01

  jmp x00,-

  ++ ret

;
; io_gets: get a CR-terminated line of
;   text from the serial port
;
; inputs
;   x78: address of first memory
;     location of result
;
io_gets:
  ; Wait for input character
  -- li x56,SER_RX
  - lb x1,$00(x56)
  beq x1,x0,$00,-

  ; Bail on carriage return
  li x2,$0d
  beq x1,x2,$00,+

  mov x2,x1,$0

  ; Echo input character
  li x56,SER_TX
  sb x2,$00(x56)

  ; Store character at next address
  sb x2,$00(x78)

  ; Increment next address
  add x78,x0,$01

  jmp x00,--

  ; Terminate string with NUL
  + li x2,$00
  sb x2,$00(x78)

  ret
