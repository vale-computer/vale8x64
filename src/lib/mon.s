; Copyright (C) 2018-2019 Francis Mahnke.

; This file is part of VALE8x64.

; VALE8x64 is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.

; VALE8x64 is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.

; You should have received a copy of the GNU General Public License
; along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.

.define CH_LF $0a
.define CH_CR $0d
.define CH_SPACE $20
.define CH_DOT $2e

.macro WAIT_TX_FREE
  ; Wait until tx free
  li x56,SER_IS_BUSY
  lb x1,$00(x56)
  bne x1,x0,$00,$fe
.endm

.macro PRINT CHAR_CODE
  ; Print character
  li x1,CHAR_CODE
  li x56,SER_TX
  sb x1,$00(x56)
.endm

; CPU breakpoint state. These locations are fixed in memory so the CPU
; knows where to access them.
.define VAL_PC_HI $0020
.define VAL_PC_LO $0021
.define VAL_SP_HI $0022
.define VAL_SP_LO $0023
.define VAL_X1 $0024
.define VAL_X2 $0025
.define VAL_X3 $0026
.define VAL_X4 $0027
.define VAL_X5 $0028
.define VAL_X6 $0029
.define VAL_X7 $002a
.define VAL_X8 $002b
.define STEP $002c

; Commands
; Memory read
CMD_MR: .byte "mr",$00
; Memory Write
CMD_MW: .byte "mw",$00
; Jump and execute
CMD_JM: .byte "jm",$00
; Step
CMD_ST: .byte "st",$00

PROMPT: .byte $0d,$0a,"> ",$00
.byte $0d,$0a,"VALE Machine Code Monitor",$0d,$0a,$00
LBL_PC: .byte "pc ",$00
LBL_SP: .byte " sp ",$00
LBL_X12: .byte " x12 ",$00
LBL_X34: .byte " x34 ",$00
LBL_X56: .byte " x56 ",$00
LBL_X78: .byte " x78 ",$00
CRLF: .byte $0d,$0a,$00

CMD_BUF: .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,
OUT_BUF: .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,
SCRATCH_BUF: .byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,

mon_start:
  call x00,print_inst
  call x00,print_state
  - call x00,input

  li x34,CMD_BUF
  li x78,CMD_MR
  li x1,$02
  call x00,strncmp
  beq x1,x0,$00,+
  li x34,CMD_BUF
  li x78,CMD_MW
  li x1,$02
  call x00,strncmp
  beq x1,x0,$00,++
  li x34,CMD_BUF
  li x78,CMD_JM
  li x1,$02
  call x00,strncmp
  beq x1,x0,$00,+++
  li x34,CMD_BUF
  li x78,CMD_ST
  li x1,$02
  call x00,strncmp
  beq x1,x0,$00,++++

  jmp x00,-

  + call x00,print_mem
  jmp x00,-
  ++ call x00,write_mem
  jmp x00,-
  +++ jmp x00,jump_addr
  ++++ jmp x00,step_addr

input:
  ; Print prompt

  li x34,PROMPT

  call x00,io_print

  ; Use command buffer address for input
  li x78,CMD_BUF

  ; Get line of input
  call x00,io_gets

  WAIT_TX_FREE
  PRINT CH_CR
  WAIT_TX_FREE
  PRINT CH_LF

  ret

;
; print_inst: print the current value of
; the program counter and the contents
; of its address (the instruction
; opcode).
;
print_inst:
  li x34,CRLF
  call x00,io_print

  WAIT_TX_FREE
  PRINT CH_DOT

  ; Print value of pc.
  li x1,$02
  li x34,VAL_PC_HI
  li x78,SCRATCH_BUF

  call x00,str_itoa

  li x34,SCRATCH_BUF
  call x00,io_print

  WAIT_TX_FREE
  PRINT CH_SPACE

  ; Print contents of pc addr (opcode).
  li x34,VAL_PC_HI
  lb x1,$00(x34)
  li x34,SCRATCH_BUF
  sb x1,$00(x34)
  li x34,VAL_PC_LO
  lb x1,$00(x34)
  li x34,SCRATCH_BUF+$01
  sb x1,$00(x34)

  call x00,print_mem_addr

  ret

print_state:
  ; Print PC header
  li x34,LBL_PC
  call x00,io_print

  ; Print PC high byte

  ; Print high nibble
  li x34,VAL_PC_HI
  lb x1,$00(x34)

  srl x1,x0,$04
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print low nibble
  li x34,VAL_PC_HI
  lb x1,$00(x34)

  and x1,x0,$0f
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print PC low byte

  ; Print high nibble
  li x34,VAL_PC_LO
  lb x1,$00(x34)

  srl x1,x0,$04
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print low nibble
  li x34,VAL_PC_LO
  lb x1,$00(x34)

  and x1,x0,$0f
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print SP header
  li x34,LBL_SP
  call x00,io_print

  ; Print SP high byte

  ; Print high nibble
  li x34,VAL_SP_HI
  lb x1,$00(x34)

  srl x1,x0,$04
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print low nibble
  li x34,VAL_SP_HI
  lb x1,$00(x34)

  and x1,x0,$0f
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print SP low byte

  ; Print high nibble
  li x34,VAL_SP_LO
  lb x1,$00(x34)

  srl x1,x0,$04
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print low nibble
  li x34,VAL_SP_LO
  lb x1,$00(x34)

  and x1,x0,$0f
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print X12 header
  li x34,LBL_X12
  call x00,io_print

  ; Print X1 high nibble
  li x34,VAL_X1
  lb x1,$00(x34)

  srl x1,x0,$04
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print X1 low nibble
  li x34,VAL_X1
  lb x1,$00(x34)

  and x1,x0,$0f
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print X2 high nibble
  li x34,VAL_X2
  lb x1,$00(x34)

  srl x1,x0,$04
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print X2 low nibble
  li x34,VAL_X2
  lb x1,$00(x34)

  and x1,x0,$0f
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print X34 header
  li x34,LBL_X34
  call x00,io_print

  ; Print X3 high nibble
  li x34,VAL_X3
  lb x1,$00(x34)

  srl x1,x0,$04
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print X3 low nibble
  li x34,VAL_X3
  lb x1,$00(x34)

  and x1,x0,$0f
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print X4 high nibble
  li x34,VAL_X4
  lb x1,$00(x34)

  srl x1,x0,$04
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print X4 low nibble
  li x34,VAL_X4
  lb x1,$00(x34)

  and x1,x0,$0f
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print X56 header
  li x34,LBL_X56
  call x00,io_print

  ; Print X5 high nibble
  li x34,VAL_X5
  lb x1,$00(x34)

  srl x1,x0,$04
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print X5 low nibble
  li x34,VAL_X5
  lb x1,$00(x34)

  and x1,x0,$0f
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print X6 high nibble
  li x34,VAL_X6
  lb x1,$00(x34)

  srl x1,x0,$04
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print X6 low nibble
  li x34,VAL_X6
  lb x1,$00(x34)

  and x1,x0,$0f
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print X78 header
  li x34,LBL_X78
  call x00,io_print

  ; Print X7 high nibble
  li x34,VAL_X7
  lb x1,$00(x34)

  srl x1,x0,$04
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print X7 low nibble
  li x34,VAL_X7
  lb x1,$00(x34)

  and x1,x0,$0f
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print X8 high nibble
  li x34,VAL_X8
  lb x1,$00(x34)

  srl x1,x0,$04
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ; Print X8 low nibble
  li x34,VAL_X8
  lb x1,$00(x34)

  and x1,x0,$0f
  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)
  call x00,io_print

  ret

print_mem:
  li x1,$02
  li x34,CMD_BUF+$03
  li x78,SCRATCH_BUF
  call x00,str_atoi

  ; Begin display of memory

  WAIT_TX_FREE
  PRINT CH_DOT

  ;
  ; Print requested address
  ;

  li x34,CMD_BUF+$03

  call x00,io_print

  WAIT_TX_FREE
  PRINT CH_SPACE

  call x00,print_mem_addr

  ret
;
; print_mem_addr: print the contents of
; an address to the serial port.
;
; inputs
;   SCRATCH_BUF: array containing the
;   high and low bytes of the address
;
print_mem_addr:
  ; Load high byte of requested address.
  li x56,SCRATCH_BUF
  lb x1,$00(x56)

  ; Push onto stack
  sub sp,x0,$01
  sb x1,$00(sp)

  ; Load low byte of requested address.
  li x56,SCRATCH_BUF+$01
  lb x1,$00(x56)

  mov x6,x1,$0

  ; Pull from stack.
  lb x1,$00(sp)
  add sp,x0,$01

  mov x5,x1,$0

  ; Load contents of requested address.
  lb x1,$00(x56)

  ; Push to stack.
  sub sp,x0,$01
  sb x1,$00(sp)

  srl x1,x0,$04

  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)

  call x00,io_print

  ; Pull from stack.
  lb x1,$00(sp)
  add sp,x0,$01

  and x1,x0,$0f

  call x00,itoa

  li x34,OUT_BUF
  sb x1,$00(x34)

  call x00,io_print

  li x34,CRLF
  call x00,io_print

  ret

write_mem:
  li x1,$02
  li x34,CMD_BUF+$03
  li x78,SCRATCH_BUF
  call x00,str_atoi

  li x1,$01
  li x34,CMD_BUF+$08
  li x78,SCRATCH_BUF+$02
  call x00,str_atoi

  ; Load high byte of destination address
  li x56,SCRATCH_BUF
  lb x1,$00(x56)

  ; Push high byte to stack
  sub sp,x0,$01
  sb x1,$00(sp)

  ; Load low byte of destination address
  li x56,SCRATCH_BUF+$01
  lb x1,$00(x56)

  ; Set low byte for store
  mov x6,x1,$0

  ; Pull high byte from stack
  lb x1,$00(sp)
  add sp,x0,$01

  ; Set high byte for store
  mov x5,x1,$0

  ; Load source byte
  li x78,SCRATCH_BUF+$02
  lb x1,$00(x78)

  ; Store source byte to destination address
  sb x1,$00(x56)

  ret

;
; pc_load: load requested address into
; CPU breakpoint state for program
; counter.
;
pc_load:
  ; Convert and store requested address.
  li x1,$02
  li x34,CMD_BUF+$03
  li x78,SCRATCH_BUF
  call x00,str_atoi

  ; Load high byte of requested address
  ; and store it to CPU breakpoint
  ; state.
  li x56,SCRATCH_BUF
  lb x1,$00(x56)

  li x34,VAL_PC_HI
  sb x1,$00(x34)

  ; Load low byte of requested address
  ; and store it to CPU breakpoint
  ; state.
  li x56,SCRATCH_BUF+$01
  lb x1,$00(x56)

  li x34,VAL_PC_LO
  sb x1,$00(x34)

  ret

;
; jump_addr: resume execution at the
; requested address.
;
jump_addr:
  li x1,$00
  li x34,STEP
  sb x1,$00(x34)

  li x34,CMD_BUF
  call x00,strlen

  beq x1,x0,$02,+

  call x00,pc_load

  + res
;
; step_addr: begin single step execution
; at the requested address.
;
step_addr:
  li x1,$01
  li x34,STEP
  sb x1,$00(x34)

  li x34,CMD_BUF
  call x00,strlen

  beq x1,x0,$02,+

  call x00,pc_load

  + res
;
; str_atoi: convert an ascii string to
; an array of integers.
;
; inputs
;   x1: number of bytes to convert
;   x34: address of first byte to
;   convert
; outputs
;   (x78): array of integers
;
str_atoi:
  ; Push number of bytes to convert
  sub sp,x0,$01
  sb x1,$00(sp)
  ; Allocate number of bytes converted
  sub sp,x0,$01
  li x1,$00
  sb x1,$00(sp)

  ; Compare number of bytes to convert with number converted.
  - lb x1,$01(sp)
  mov x2,x1,$00
  lb x1,$00(sp)

  ; Reached number to convert. Finished.
  beq x1,x2,$00,+

  ; Increment number of bytes converted.
  add x1,x0,$01
  sb x1,$00(sp)

  ; Load bits 15-12
  lb x1,$00(x34)
  ; Convert to int and shift left
  call x00,atoi
  li x2,$04
  sll x1,x1,x2

  ; Push bits 15-12 onto stack
  sub sp,x0,$01
  sb x1,$00(sp)

  ; Load 11-8
  add x34,x0,$01

  lb x1,$00(x34)
  ; Convert to int
  call x00,atoi

  ; Pull bits 15-12 off stack
  lb x2,$00(sp)
  add sp,x0,$01

  ; Or 15-2 with 11-8 to get high byte of address
  orr x1,x1,x2

  ; Store high byte of address
  sb x1,$00(x78)

  add x34,x0,$01
  add x78,x0,$01

  jmp x00,-

  ; Restore stack
  + add sp,x0,$02
  ret

;
; str_itoa: convert an array of integers
; to an ascii string.
;
; inputs
;   x1: number of bytes to convert
;   x34: address of first byte to
;   convert
; outputs
;   (x78): ascii string
;
str_itoa:
  ; Push number of bytes to convert
  sub sp,x0,$01
  sb x1,$00(sp)
  ; Allocate number of bytes converted
  sub sp,x0,$01
  li x1,$00
  sb x1,$00(sp)

  ; Compare number of bytes to convert
  ; with number converted
  - lb x1,$01(sp)
  mov x2,x1,$00
  lb x1,$00(sp)

  ; Reached number to convert. Finished.
  beq x1,x2,$00,+

  ; Increment number of bytes converted
  add x1,x0,$01
  sb x1,$00(sp)

  ; Load integer
  lb x1,$00(x34)

  ; Push integer onto stack
  sub sp,x0,$01
  sb x1,$00(sp)

  ; Shift right, convert high byte
  ; to ascii and store
  srl x1,x0,$04
  call x00,itoa
  sb x1,$00(x78)

  ; Increment output pointer
  add x78,x0,$01

  ; Pull integer off stack
  lb x1,$00(sp)
  add sp,x0,$01

  ; Convert low byte to ascii and store
  and x1,x0,$0f
  call x00,itoa
  sb x1,$00(x78)

  add x34,x0,$01
  add x78,x0,$01

  jmp x00,-

  ; Restore stack
  + add sp,x0,$02
  ret

.include "io.s"
.include "std.s"
.include "string.s"
