; Copyright (C) 2018-2019 Francis Mahnke.

; This file is part of VALE8x64.

; VALE8x64 is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.

; VALE8x64 is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.

; You should have received a copy of the GNU General Public License
; along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.

;
; Convert the ascii code of the hexadecimal digit stored in x1 to its
; integer value. Store the result in x1.
;
; Mutates: x1,x2
;
atoi:
  ; '0'
  li x2,$30
  beq x1,x2,$00,+
  ; '1'
  li x2,$31
  beq x1,x2,$00,++
  ; '2'
  li x2,$32
  beq x1,x2,$00,+++
  ; '3'
  li x2,$33
  beq x1,x2,$00,++++
  ; '4'
  li x2,$34
  beq x1,x2,$00,+++++
  ; '5'
  li x2,$35
  beq x1,x2,$00,++++++
  ; '6'
  li x2,$36
  beq x1,x2,$00,+++++++
  ; '7'
  li x2,$37
  beq x1,x2,$00,++++++++
  ; '8'
  li x2,$38
  beq x1,x2,$00,+++++++++
  ; '9'
  li x2,$39
  beq x1,x2,$00,++++++++++
  ; 'a'
  li x2,$61
  beq x1,x2,$00,+++++++++++
  ; 'b'
  li x2,$62
  beq x1,x2,$00,++++++++++++
  ; 'c'
  li x2,$63
  beq x1,x2,$00,+++++++++++++
  ; 'd'
  li x2,$64
  beq x1,x2,$00,++++++++++++++
  ; 'e'
  li x2,$65
  beq x1,x2,$00,+++++++++++++++
  ; 'f'
  li x2,$66
  beq x1,x2,$00,++++++++++++++++

  + li x1,$00
  ret
  ++ li x1,$01
  ret
  +++ li x1,$02
  ret
  ++++ li x1,$03
  ret
  +++++ li x1,$04
  ret
  ++++++ li x1,$05
  ret
  +++++++ li x1,$06
  ret
  ++++++++ li x1,$07
  ret
  +++++++++ li x1,$08
  ret
  ++++++++++ li x1,$09
  ret
  +++++++++++ li x1,$0a
  ret
  ++++++++++++ li x1,$0b
  ret
  +++++++++++++ li x1,$0c
  ret
  ++++++++++++++ li x1,$0d
  ret
  +++++++++++++++ li x1,$0e
  ret
  ++++++++++++++++ li x1,$0f
  ret

;
; Convert the number stored in x1 to its corresponding ascii value. Store the
; result in x1.
;
; Mutates: x1,x2
;
itoa:
  ; 0
  li x2,$00
  beq x1,x2,$00,+
  ; 1
  li x2,$01
  beq x1,x2,$00,++
  ; 2
  li x2,$02
  beq x1,x2,$00,+++
  ; 3
  li x2,$03
  beq x1,x2,$00,++++
  ; 4
  li x2,$04
  beq x1,x2,$00,+++++
  ; 5
  li x2,$05
  beq x1,x2,$00,++++++
  ; 6
  li x2,$06
  beq x1,x2,$00,+++++++
  ; 7
  li x2,$07
  beq x1,x2,$00,++++++++
  ; 8
  li x2,$08
  beq x1,x2,$00,+++++++++
  ; 9
  li x2,$09
  beq x1,x2,$00,++++++++++
  ; a
  li x2,$0a
  beq x1,x2,$00,+++++++++++
  ; b
  li x2,$0b
  beq x1,x2,$00,++++++++++++
  ; c
  li x2,$0c
  beq x1,x2,$00,+++++++++++++
  ; d
  li x2,$0d
  beq x1,x2,$00,++++++++++++++
  ; e
  li x2,$0e
  beq x1,x2,$00,+++++++++++++++
  ; f
  li x2,$0f
  beq x1,x2,$00,++++++++++++++++

  + li x1,$30
  ret
  ++ li x1,$31
  ret
  +++ li x1,$32
  ret
  ++++ li x1,$33
  ret
  +++++ li x1,$34
  ret
  ++++++ li x1,$35
  ret
  +++++++ li x1,$36
  ret
  ++++++++ li x1,$37
  ret
  +++++++++ li x1,$38
  ret
  ++++++++++ li x1,$39
  ret
  +++++++++++ li x1,$61
  ret
  ++++++++++++ li x1,$62
  ret
  +++++++++++++ li x1,$63
  ret
  ++++++++++++++ li x1,$64
  ret
  +++++++++++++++ li x1,$65
  ret
  ++++++++++++++++ li x1,$66
  ret
