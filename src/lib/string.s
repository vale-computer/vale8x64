; Copyright (C) 2018-2019 Francis Mahnke.

; This file is part of VALE8x64.

; VALE8x64 is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.

; VALE8x64 is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.

; You should have received a copy of the GNU General Public License
; along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.

;
; strlen: Return the length of the
;   string in bytes.
;
; input
;   x34: Address of the first character
;     of the string.
; output
;   x1: Length of the string.
;
strlen:
  li x2,$00

  - lb x1,$00(x34)
  beq x1,x0,$00,++

  add x2,x0,$01

  add x34,x0,$01

  + jmp x00,-

  ++ mov x1,x2,$00

  ret

;
; strncmp: Compare two strings.
;
; input
;   x1: Number of characters to compare.
;   x34: Address of the first string.
;   x78: Address of the second string.
;
; output
;   x1: 0 if strings are equal, 1 if
;     not.
;
strncmp:
  ; Push number of bytes to compare to stack
  sub sp,x0,$01
  sb x1,$00(sp)
  ; Allocate number of bytes compared on stack
  sub sp,x0,$01
  li x1,$00
  sb x1,$00(sp)

  ; Compare number of bytes to compare with number compared.
  - lb x1,$01(sp)
  mov x2,x1,$0
  lb x1,$00(sp)

  ; Reached number to compare. Strings are equal.
  beq x1,x2,$00,++

  ; Increment number of bytes compared.
  add x1,x0,$01
  sb x1,$00(sp)

  ; Compare the next bytes of each string.

  lb x1,$00(x34)
  mov x2,x1,$0
  lb x1,$00(x78)

  ; Bytes are not equal. Strings are inequal.
  bne x1,x2,$00,+

  ; Increment pointer to the next character of each string.
  add x34,x0,$01
  add x78,x0,$01

  jmp x00,-

  + mov x1,x0,$01
  jmp x00,+++
  ++ mov x1,x0,$00
  ; Restore stack
  +++ add sp,x0,$02

  ret
