/*
Copyright (C) 2018-2019 Francis Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

`default_nettype none

module vale_up5k(
    input clk,
    // Serial I/O
    output io_48b,
    input io_51a,
    // Debug
    output io_42b,
    output io_43a,
    output io_38b,
    output io_39a,
    output io_13b,
    output io_20a,
    output io_31b,
    output io_29b
);

/*
Vale computer
*/

reg vale_clr;

vale vale(
    .clk(clk),
    .clr(vale_clr),
    .rs232_tx_out(io_48b),
    .rs232_rx_in(io_51a),
    .dbg_0(io_42b),
    .dbg_1(io_43a),
    .dbg_2(io_38b),
    .dbg_3(io_39a),
    .dbg_4(io_13b),
    .dbg_5(io_20a),
    .dbg_6(io_31b),
    .dbg_7(io_29b)
);

/*
Startup and clear
*/

localparam clr_size = 7;
initial vale_clr = 1;
reg [clr_size : 0] clr_cnt = 0;
localparam clr_max = (1 << 7) - 1;

always @(posedge clk) begin
    if (clr_cnt != clr_max) begin
        clr_cnt <= clr_cnt + 1;
    end else begin
        vale_clr <= 0;
    end
end

endmodule
