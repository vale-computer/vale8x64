module clock_div(
    input clk_master,
    output clk
);

wire runstop;
assign runstop = 0;
wire reset_clk2;
reg [15:0] cnt1;
reg [6:0]  cnt2;
reg [2:0]  dec_cntr;
reg half_sec_pulse;
initial cnt1 = 0;
initial cnt2 = 0;
//initial dec_cntr = 0;
initial half_sec_pulse = 0;

assign clk = dec_cntr;

always@(posedge clk_master)
  begin
     if (reset_clk2==1'b1)
       begin
      cnt1<=0;
          cnt2<=0;
      half_sec_pulse<=0;
      dec_cntr<=0;
       end
     else if (runstop==1'b0)  // don't do anything unless enabled
       begin
      cnt1 <= cnt1 + 1;
      if (cnt1 == 0) 
        if (cnt2 == 21) 
          begin
         cnt2 <= 0;
         half_sec_pulse <= 1;  
          end
        else
          cnt2 <= cnt2 + 1;
      else
        half_sec_pulse <= 0;
      
      if (half_sec_pulse == 1)	
        dec_cntr <= dec_cntr + 1; // count half seconds
// note: dec_cntr>>1 is a seconds timer, but dec_cntr is only 2 bits so 0-3...
       end
  end	

endmodule
