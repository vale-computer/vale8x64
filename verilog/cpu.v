/*
Copyright (C) 2018-2019 Francis Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

`default_nettype none

`include "interrupt.v"
`include "opcodes.v"

`define MONITOR_ENTRY 16'h0010
`define MON_BRK_STATE 16'h0020
`define STACK_START 16'h01ff

module cpu(
    input clk,
    input clr,
    output dbg_halted,
    output [15:0] io_addr,
    input [7:0] io_data_in,
    output reg [7:0] io_data_out,
    output reg io_read,
    output reg io_load,
    input [1:0] interrupt,
    input [7:0] interrupt_addr,
    output [7:0] dbg_pc_out,
    output [7:0] dbg_ir_out,
    output [7:0] dbg_x1_out,
    output [7:0] dbg_x2_out,
    output [7:0] dbg_x3_out,
    output [7:0] dbg_x4_out,
    output [7:0] dbg_x5_out,
    output [7:0] dbg_x6_out,
    output [7:0] dbg_x7_out,
    output [7:0] dbg_x8_out
);

/*
ALU
*/

reg [7:0] alu_in_0;
reg [7:0] alu_in_1;
wire [7:0] alu_out;
wire alu_carry;

alu alu(
    .in_0(alu_in_0),
    .in_1(alu_in_1),
    .out(alu_out),
    .carry(alu_carry)
);

reg [3:0] tstate;
initial tstate = 0;

reg [15:0] sp;
initial sp = `STACK_START;
reg [15:0] pc;
initial pc = 0;

reg [15:0] mar;
reg [7:0] ir;

reg [7:0] x1;
reg [7:0] x2;
reg [7:0] x3;
reg [7:0] x4;
reg [7:0] x5;
reg [7:0] x6;
reg [7:0] x7;
reg [7:0] x8;
reg [7:0] tmp;
reg [7:0] tmp2;

// Debug
assign dbg_pc_out = pc[7:0];
assign dbg_ir_out = ir;
assign dbg_x1_out = x1;
assign dbg_x2_out = x2;
assign dbg_x3_out = x3;
assign dbg_x4_out = x4;
assign dbg_x5_out = x5;
assign dbg_x6_out = x6;
assign dbg_x7_out = x7;
assign dbg_x8_out = x8;
assign dbg_halted = 1;

reg instr_end;
initial instr_end = 0;

`define STEP_OFF 0
`define STEP_NEXT_INST 1
`define STEP_THIS_INST 2

reg [1:0] single_step;
initial single_step = `STEP_OFF;

reg halted;
initial halted = 0;

reg branch;
wire [15:0] branch_offset;
assign branch_offset = io_data_in > 127 ? - (256 - io_data_in) :
    io_data_in;

reg pc_count;
reg pc_count2;
reg pc_dec2;
reg sp_dec;

reg mar_load_pc;
reg mar_load_sp;
reg prog_ram_load_pc_hi;
reg prog_ram_load_pc_lo;
reg ir_load_prog_ram;

reg [1:0] _interrupt = 0;
reg [7:0] _interrupt_addr;

assign io_addr = mar;

always @* begin
    branch <= 0;

    pc_count <= 0;
    pc_count2 <= 0;
    pc_dec2 <= 0;
    sp_dec <= 0;

    mar_load_pc <= 0;
    mar_load_sp <= 0;
    prog_ram_load_pc_hi <= 0;
    prog_ram_load_pc_lo <= 0;
    ir_load_prog_ram <= 0;

    if (_interrupt == `INT_THIS_INST) begin
        if (tstate == 0) begin
            sp_dec <= 1;
        end else if (tstate == 1) begin
            mar_load_sp <= 1;
            sp_dec <= 1;
            prog_ram_load_pc_hi <= 1;
        end else if (tstate == 2) begin
            mar_load_sp <= 1;
            prog_ram_load_pc_lo <= 1;
        end
    end else begin
        if (tstate == 0) begin
            pc_count <= 1;
            mar_load_pc <= 1;
        end else if (tstate == 1) begin
            ir_load_prog_ram <= 1;
        end
    end

    if (ir >= `OP_LI_X1_IMM && ir <= `OP_LI_X8_IMM) begin
        if (tstate == 2) begin
            mar_load_pc <= 1;
            pc_count <= 1;
        end
    end

    if (ir == `OP_CALL_X00_OFFSET || ir == `OP_CALL_X34_OFFSET) begin
        if (tstate == 2) begin
            pc_count2 <= 1;
            sp_dec <= 1;
        end else if (tstate == 3) begin
            mar_load_sp <= 1;
            sp_dec <= 1;
            prog_ram_load_pc_hi <= 1;
        end else if (tstate == 4) begin
            mar_load_sp <= 1;
            prog_ram_load_pc_lo <= 1;
            pc_dec2 <= 1;
        end
    end

    if (ir == `OP_BEQ_X1_X0_IMM_OFFSET) begin
        if (tstate == 2) begin
            mar_load_pc <= 1;
            pc_count <= 1;
        end else if (tstate == 4) begin
            if (x1 == tmp) begin
                branch <= 1;
            end else begin
                pc_count <= 1;
            end
        end
    end else if (ir == `OP_BEQ_X1_X2_IMM_OFFSET) begin
        if (tstate == 2) begin
            mar_load_pc <= 1;
            pc_count <= 1;
        end else if (tstate == 4) begin
            if (x1 == (x2 + tmp)) begin
                branch <= 1;
            end else begin
                pc_count <= 1;
            end
        end
    end else if (ir == `OP_BNE_X1_X0_IMM_OFFSET) begin
        if (tstate == 2) begin
            mar_load_pc <= 1;
            pc_count <= 1;
        end else if (tstate == 4) begin
            if (x1 != tmp) begin
                branch <= 1;
            end else begin
                pc_count <= 1;
            end
        end
    end else if (ir == `OP_BNE_X1_X2_IMM_OFFSET) begin
        if (tstate == 2) begin
            mar_load_pc <= 1;
            pc_count <= 1;
        end else if (tstate == 4) begin
            if (x1 != (x2 + tmp)) begin
                branch <= 1;
            end else begin
                pc_count <= 1;
            end
        end
    end else if (ir == `OP_BLT_X1_X0_IMM_OFFSET) begin
        if (tstate == 2) begin
            mar_load_pc <= 1;
            pc_count <= 1;
        end else if (tstate == 4) begin
            if (x1 < tmp) begin
                branch <= 1;
            end else begin
                pc_count <= 1;
            end
        end
    end else if (ir == `OP_BLT_X1_X2_IMM_OFFSET) begin
        if (tstate == 2) begin
            mar_load_pc <= 1;
            pc_count <= 1;
        end else if (tstate == 4) begin
            if (x1 < x2 + tmp) begin
                branch <= 1;
            end else begin
                pc_count <= 1;
            end
        end
    end

    if (ir >= `OP_LB_X1_OFFSET_X34_ && ir <= `OP_SB_X2_OFFSET_SP_) begin
        if (tstate == 2) begin
            mar_load_pc <= 1;
            pc_count <= 1;
        end
    end

    if (ir >= `OP_MOV_X1_X0_IMM && ir <= `OP_MOV_X8_X1_IMM) begin
        if (tstate == 2) begin
            mar_load_pc <= 1;
            pc_count <= 1;
        end
    end

    case (tstate)
        3: begin
            case (ir)
                `OP_MOV_X1_X0_IMM: begin
                    alu_in_0 <= 0;
                    alu_in_1 <= io_data_in;
                end
                `OP_MOV_X1_X1_IMM: begin
                    alu_in_0 <= x1;
                    alu_in_1 <= io_data_in;
                end
                `OP_MOV_X1_X2_IMM: begin
                    alu_in_0 <= x2;
                    alu_in_1 <= io_data_in;
                end
                `OP_MOV_X1_X3_IMM: begin
                    alu_in_0 <= x3;
                    alu_in_1 <= io_data_in;
                end
                `OP_MOV_X1_X4_IMM: begin
                    alu_in_0 <= x4;
                    alu_in_1 <= io_data_in;
                end
                `OP_MOV_X1_X5_IMM: begin
                    alu_in_0 <= x5;
                    alu_in_1 <= io_data_in;
                end
                `OP_MOV_X1_X6_IMM: begin
                    alu_in_0 <= x6;
                    alu_in_1 <= io_data_in;
                end
                `OP_MOV_X1_X7_IMM: begin
                    alu_in_0 <= x7;
                    alu_in_1 <= io_data_in;
                end
                `OP_MOV_X1_X8_IMM: begin
                    alu_in_0 <= x8;
                    alu_in_1 <= io_data_in;
                end
                `OP_MOV_X2_X1_IMM: begin
                    alu_in_0 <= x1;
                    alu_in_1 <= io_data_in;
                end
                `OP_MOV_X2_X4_IMM: begin
                    alu_in_0 <= x4;
                    alu_in_1 <= io_data_in;
                end
                `OP_MOV_X2_X5_IMM: begin
                    alu_in_0 <= x5;
                    alu_in_1 <= io_data_in;
                end
                `OP_MOV_X3_X1_IMM: begin
                    alu_in_0 <= x1;
                    alu_in_1 <= io_data_in;
                end
                `OP_MOV_X4_X1_IMM: begin
                    alu_in_0 <= x1;
                    alu_in_1 <= io_data_in;
                end
                `OP_MOV_X5_X1_IMM: begin
                    alu_in_0 <= x1;
                    alu_in_1 <= io_data_in;
                end
                `OP_MOV_X6_X1_IMM: begin
                    alu_in_0 <= x1;
                    alu_in_1 <= io_data_in;
                end
                `OP_MOV_X7_X1_IMM: begin
                    alu_in_0 <= x1;
                    alu_in_1 <= io_data_in;
                end
                `OP_MOV_X8_X1_IMM: begin
                    alu_in_0 <= x1;
                    alu_in_1 <= io_data_in;
                end
            endcase
        end
    endcase

    /*
    Addition/Subtraction
    */

    if (ir >= `OP_ADD_X1_X0_IMM && ir <= `OP_SUB_SP_X0_IMM) begin
        if (tstate == 2) begin
            mar_load_pc <= 1;
            pc_count <= 1;
        end
    end

    case (tstate)
        3: begin
            case (ir)
                `OP_ADD_X1_X0_IMM: begin
                    alu_in_0 <= x1;
                    alu_in_1 <= io_data_in;
                end
                `OP_ADD_X2_X0_IMM: begin
                    alu_in_0 <= x2;
                    alu_in_1 <= io_data_in;
                end
                `OP_ADD_X5_X0_IMM: begin
                    alu_in_0 <= x5;
                    alu_in_1 <= io_data_in;
                end
                `OP_ADD_X6_X0_IMM: begin
                    alu_in_0 <= x6;
                    alu_in_1 <= io_data_in;
                end
                `OP_ADD_X34_X0_IMM: begin
                    alu_in_0 <= x4;
                    alu_in_1 <= io_data_in;
                end
                `OP_ADD_X78_X0_IMM: begin
                    alu_in_0 <= x8;
                    alu_in_1 <= io_data_in;
                end
            endcase
        end
    endcase

    case (tstate)
        4: begin
            case (ir)
                `OP_ADD_X34_X0_IMM: begin
                    alu_in_0 <= x3;
                    alu_in_1 <= tmp;
                end
                `OP_ADD_X78_X0_IMM: begin
                    alu_in_0 <= x7;
                    alu_in_1 <= tmp;
                end
            endcase
        end
    endcase

    /*
    Bit operations
    */

    if (ir >= `OP_AND_X1_X0_IMM && ir <= `OP_SRL_X1_X0_IMM) begin
        if (tstate == 2) begin
            mar_load_pc <= 1;
            pc_count <= 1;
        end
    end
end

always @(posedge clk) begin
    if (!clr && !halted) begin
        tstate <= tstate + 1;
        halted <= 0;
        single_step <= single_step;
        instr_end <= instr_end;

        _interrupt <= _interrupt;
        _interrupt_addr <= _interrupt_addr;

        io_read <= 0;
        io_load <= 0;
        io_data_out <= io_data_out;

        ir <= ir;
        mar <= mar;
        pc <= pc;
        x1 <= x1;
        x2 <= x2;
        x3 <= x3;
        x4 <= x4;
        x5 <= x5;
        x6 <= x6;
        x7 <= x7;
        x8 <= x8;
        tmp <= tmp;

        if (interrupt) begin
            _interrupt <= interrupt;
            _interrupt_addr <= interrupt_addr;
        end

        if (mar_load_pc) begin
            mar <= pc;
        end
        if (mar_load_sp) begin
            mar <= sp;
        end

        if (ir_load_prog_ram) begin
            ir <= io_data_in;
        end

        if (pc_count && single_step != `STEP_THIS_INST) begin
            pc <= pc + 1;
        end
        if (pc_count2 && single_step != `STEP_THIS_INST) begin
            pc <= pc + 2;
        end
        if (pc_dec2) begin
            pc <= pc - 2;
        end
        if (sp_dec) begin
            sp <= sp - 1;
        end

        if (branch) begin
            pc <= pc + branch_offset - 2;
        end

        if (instr_end) begin
            instr_end <= 0;

            if (single_step == `STEP_NEXT_INST) begin
                single_step <= `STEP_THIS_INST;
            end
            if (_interrupt == `INT_NEXT_INST) begin
                _interrupt <= `INT_THIS_INST;
            end
        end

        if (prog_ram_load_pc_hi) begin
            io_data_out <= pc >> 8;
            io_load <= 1;
        end
        if (prog_ram_load_pc_lo) begin
            io_data_out <= pc & 8'hff;
            io_load <= 1;
        end

        case (ir)
            `OP_LB_X1_OFFSET_X34_, `OP_LB_X1_OFFSET_X56_,
            `OP_LB_X1_OFFSET_X78_, `OP_LB_X1_OFFSET_SP_: begin
                case (tstate)
                    3: begin
                        io_read <= 1;
                    end
                    4: begin
                        x1 <= io_data_in;
                        instr_end <= 1;
                    end
                endcase
            end
            `OP_LB_X2_OFFSET_X34_, `OP_LB_X2_OFFSET_X56_,
            `OP_LB_X2_OFFSET_X78_, `OP_LB_X2_OFFSET_SP_: begin
                case (tstate)
                    3: begin
                        io_read <= 1;
                    end
                    4: begin
                        x2 <= io_data_in;
                        instr_end <= 1;
                    end
                endcase
            end
        endcase

        case (ir)
            `OP_SB_X1_OFFSET_X34_, `OP_SB_X1_OFFSET_X56_,
            `OP_SB_X1_OFFSET_X78_, `OP_SB_X1_OFFSET_SP_: begin
                io_data_out <= x1;
            end
            `OP_SB_X2_OFFSET_X34_, `OP_SB_X2_OFFSET_X56_,
            `OP_SB_X2_OFFSET_X78_, `OP_SB_X2_OFFSET_SP_: begin
                io_data_out <= x2;
            end
        endcase

        case (ir)
            `OP_SB_X1_OFFSET_X34_, `OP_SB_X2_OFFSET_X34_,
            `OP_SB_X1_OFFSET_X56_, `OP_SB_X2_OFFSET_X56_,
            `OP_SB_X1_OFFSET_X78_, `OP_SB_X2_OFFSET_X78_,
            `OP_SB_X1_OFFSET_SP_, `OP_SB_X2_OFFSET_SP_: begin
                case (tstate)
                    4: begin
                        io_load <= 1;
                        instr_end <= 1;
                    end
                endcase
            end
        endcase

        if (_interrupt == `INT_THIS_INST) begin
            if (tstate == 0) begin
                ir <= `OP_NOP;
            end else if (tstate == 3) begin
                pc <= _interrupt_addr;

                instr_end <= 1;
            end else if (tstate == 4) begin
                _interrupt <= `INT_OFF;

                tstate <= 0;
            end
        end else if (ir == `OP_BRK || single_step == `STEP_THIS_INST) begin
            if (tstate == 2) begin
                mar <= `MON_BRK_STATE;

                io_data_out <= pc >> 8;
                io_load <= 1;
            end else if (tstate == 3) begin
                mar <= mar + 1;

                io_data_out <= pc;
                io_load <= 1;
            end else if (tstate == 4) begin
                mar <= mar + 1;

                io_data_out <= sp >> 8;
                io_load <= 1;
            end else if (tstate == 5) begin
                mar <= mar + 1;

                io_data_out <= sp;
                io_load <= 1;
            end else if (tstate == 6) begin
                mar <= mar + 1;

                io_data_out <= x1;
                io_load <= 1;
            end else if (tstate == 7) begin
                mar <= mar + 1;

                io_data_out <= x2;
                io_load <= 1;
            end else if (tstate == 8) begin
                mar <= mar + 1;

                io_data_out <= x3;
                io_load <= 1;
            end else if (tstate == 9) begin
                mar <= mar + 1;

                io_data_out <= x4;
                io_load <= 1;
            end else if (tstate == 10) begin
                mar <= mar + 1;

                io_data_out <= x5;
                io_load <= 1;
            end else if (tstate == 11) begin
                mar <= mar + 1;

                io_data_out <= x6;
                io_load <= 1;

            end else if (tstate == 12) begin
                mar <= mar + 1;

                io_data_out <= x7;
                io_load <= 1;

            end else if (tstate == 13) begin
                mar <= mar + 1;

                io_data_out <= x8;
                io_load <= 1;

            end else if (tstate == 14) begin
                mar <= mar + 1;

                single_step <= `STEP_OFF;

                pc <= `MONITOR_ENTRY;
            end else if (tstate == 15) begin
                tstate <= 0;
            end
        end else if (ir >= `OP_LI_X1_IMM && ir <= `OP_LI_X8_IMM) begin
            if (tstate == 3) begin
                if (ir == `OP_LI_X1_IMM) begin
                    x1 <= io_data_in;
                end else if (ir == `OP_LI_X2_IMM) begin
                    x2 <= io_data_in;
                end else if (ir == `OP_LI_X3_IMM) begin
                    x3 <= io_data_in;
                end else if (ir == `OP_LI_X4_IMM) begin
                    x4 <= io_data_in;
                end else if (ir == `OP_LI_X5_IMM) begin
                    x5 <= io_data_in;
                end else if (ir == `OP_LI_X6_IMM) begin
                    x6 <= io_data_in;
                end else if (ir == `OP_LI_X7_IMM) begin
                    x7 <= io_data_in;
                end else begin // `OP_LI_X8_IMM
                    x8 <= io_data_in;
                end 

                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir >= `OP_JMP_X00_OFFSET  && ir <= `OP_JMP_X34_OFFSET) begin
            if (tstate == 2) begin
                mar <= pc;
                pc <= pc + 1;
            end else if (tstate == 3) begin
                tmp <= io_data_in;
                mar <= pc;
            end else if (tstate == 4) begin
                if (ir == `OP_JMP_X00_OFFSET) begin
                    pc <= io_data_in << 8 | tmp;
                end else begin // `OP_JMP_X34_OFFSET
                    pc <= (x3 << 8 | x4) + (io_data_in << 8 | tmp);
                end

                instr_end <= 1;
            end else if (tstate == 5) begin
                tstate <= 0;
            end
        end else if (ir >= `OP_CALL_X00_OFFSET && ir <= `OP_CALL_X34_OFFSET) begin
            if (tstate == 5) begin
                mar <= pc;
                pc <= pc + 1;
            end else if (tstate == 6) begin
                mar <= pc;
                tmp <= io_data_in;
            end else if (tstate == 7) begin
                if (ir == `OP_CALL_X00_OFFSET) begin
                    pc <= io_data_in << 8 | tmp;
                end else begin // `OP_CALL_X34_OFFSET
                    pc <= (x3 << 8 | x4) + (io_data_in << 8 | tmp);
                end

                instr_end <= 1;
            end else if (tstate == 8) begin
                tstate <= 0;
            end
        end else if (ir == `OP_RET) begin
            if (tstate == 2) begin
                mar <= sp;
                sp <= sp + 1;
            end else if (tstate == 3) begin
                mar <= sp;
                tmp <= io_data_in;
                sp <= sp + 1;
            end else if (tstate == 4) begin
                pc <= io_data_in << 8 | tmp;

                instr_end <= 1;
            end else if (tstate == 5) begin
                tstate <= 0;
            end
        end else if (ir >= `OP_BEQ_X1_X0_IMM_OFFSET &&
            ir <= `OP_BLT_X1_X2_IMM_OFFSET) begin

            if (tstate == 3) begin
                tmp <= io_data_in;

                mar <= pc;

                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir >= `OP_LB_X1_OFFSET_X34_ && ir <= `OP_LB_X2_OFFSET_X34_) begin
            if (tstate == 3) begin
                mar <= io_data_in + (x3 << 8 | x4);
            end else if (tstate > 4) begin
                tstate <= 0;
            end
        end else if (ir >= `OP_LB_X1_OFFSET_X56_ && ir <= `OP_LB_X2_OFFSET_X56_) begin
            if (tstate == 3) begin
                mar <= io_data_in + (x5 << 8 | x6);
            end else if (tstate > 4) begin
                tstate <= 0;
            end
        end else if (ir >= `OP_LB_X1_OFFSET_X78_ && ir <= `OP_LB_X2_OFFSET_X78_) begin
            if (tstate == 3) begin
                mar <= io_data_in + (x7 << 8 | x8);
            end else if (tstate > 4) begin
                tstate <= 0;
            end
        end else if (ir >= `OP_LB_X1_OFFSET_SP_ && ir <= `OP_LB_X2_OFFSET_SP_) begin
            if (tstate == 3) begin
                mar <= io_data_in + sp;
            end else if (tstate > 4) begin
                tstate <= 0;
            end
        end else if (ir >= `OP_SB_X1_OFFSET_X34_ && ir <= `OP_SB_X2_OFFSET_X34_) begin
            if (tstate == 3) begin
                mar <= io_data_in + (x3 << 8 | x4);
            end else if (tstate > 4) begin
                tstate <= 0;
            end
        end else if (ir >= `OP_SB_X1_OFFSET_X56_ && ir <= `OP_SB_X2_OFFSET_X56_) begin
            if (tstate == 3) begin
                mar <= io_data_in + (x5 << 8 | x6);
            end else if (tstate > 4) begin
                tstate <= 0;
            end
        end else if (ir >= `OP_SB_X1_OFFSET_X78_ && ir <= `OP_SB_X2_OFFSET_X78_) begin
            if (tstate == 3) begin
                mar <= io_data_in + (x7 << 8 | x8);
            end else if (tstate > 4) begin
                tstate <= 0;
            end
        end else if (ir >= `OP_SB_X1_OFFSET_SP_ && ir <= `OP_SB_X2_OFFSET_SP_) begin
            if (tstate == 3) begin
                mar <= io_data_in + sp;
            end else if (tstate > 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_MOV_X1_X0_IMM) begin
            if (tstate == 3) begin
                x1 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_MOV_X1_X1_IMM) begin
            if (tstate == 3) begin
                x1 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_MOV_X1_X2_IMM) begin
            if (tstate == 3) begin
                x1 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_MOV_X1_X3_IMM) begin
            if (tstate == 3) begin
                x1 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_MOV_X1_X4_IMM) begin
            if (tstate == 3) begin
                x1 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_MOV_X1_X5_IMM) begin
            if (tstate == 3) begin
                x1 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_MOV_X1_X6_IMM) begin
            if (tstate == 3) begin
                x1 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_MOV_X1_X7_IMM) begin
            if (tstate == 3) begin
                x1 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_MOV_X1_X8_IMM) begin
            if (tstate == 3) begin
                x1 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_MOV_X2_X1_IMM) begin
            if (tstate == 3) begin
                x2 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_MOV_X2_X4_IMM) begin
            if (tstate == 3) begin
                x2 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_MOV_X2_X5_IMM) begin
            if (tstate == 3) begin
                x2 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_MOV_X3_X1_IMM) begin
            if (tstate == 3) begin
                x3 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_MOV_X4_X1_IMM) begin
            if (tstate == 3) begin
                x4 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_MOV_X5_X1_IMM) begin
            if (tstate == 3) begin
                x5 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_MOV_X6_X1_IMM) begin
            if (tstate == 3) begin
                x6 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_MOV_X7_X1_IMM) begin
            if (tstate == 3) begin
                x7 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_MOV_X8_X1_IMM) begin
            if (tstate == 3) begin
                x8 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_ADD_X1_X0_IMM) begin
            if (tstate == 3) begin
                x1 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_ADD_X1_X2_IMM) begin
            if (tstate == 3) begin
                x1 <= x1 + x2 + io_data_in;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_ADD_X2_X0_IMM) begin
            if (tstate == 3) begin
                x2 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_ADD_X5_X0_IMM) begin
            if (tstate == 3) begin
                x5 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_ADD_X6_X0_IMM) begin
            if (tstate == 3) begin
                x6 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_ADD_X34_X0_IMM) begin
            if (tstate == 3) begin
                x4 <= alu_out;
                tmp <= alu_carry;
            end else if (tstate == 4) begin
                x3 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 5) begin
                tstate <= 0;
            end
        end else if (ir == `OP_ADD_X78_X0_IMM) begin
            if (tstate == 3) begin
                x8 <= alu_out;
                tmp <= alu_carry;
            end else if (tstate == 4) begin
                x7 <= alu_out;
                instr_end <= 1;
            end else if (tstate == 5) begin
                tstate <= 0;
            end
        end else if (ir == `OP_ADD_SP_X0_IMM) begin
            if (tstate == 3) begin
                sp <= sp + io_data_in;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_SUB_SP_X0_IMM) begin
            if (tstate == 3) begin
                sp <= sp - io_data_in;
                instr_end <= 1;
            end else if (tstate == 4) begin
                tstate <= 0;
            end
        end else if (ir == `OP_SLL_X1_X1_X2) begin
            if (tstate == 2) begin
                x1 <= x1 << x2;

                instr_end <= 1;
            end else if (tstate == 3) begin
                tstate <= 0;
            end
        end else if (ir == `OP_ORR_X1_X1_X2) begin
            if (tstate == 2) begin
                x1 <= x1 | x2;

                instr_end <= 1;
            end else if (tstate == 3) begin
                tstate <= 0;
            end
        end else if (ir == `OP_AND_X1_X0_IMM) begin
            if (tstate == 3) begin
                x1 <= x1 & io_data_in;

                instr_end <= 1;
            end else if (tstate > 3) begin
                tstate <= 0;
            end
        end else if (ir == `OP_SRL_X1_X0_IMM) begin
            if (tstate == 3) begin
                x1 <= x1 >> io_data_in;

                instr_end <= 1;
            end else if (tstate > 3) begin
                tstate <= 0;
            end
        end else if (ir == `OP_RES) begin
            if (tstate == 2) begin
                mar <= `MON_BRK_STATE;
            end else if (tstate == 3) begin
                tmp <= io_data_in;

                mar <= mar + 1;
            end else if (tstate == 4) begin
                pc <= tmp << 8 | io_data_in;

                mar <= mar + 1;
            end else if (tstate == 5) begin
                tmp <= io_data_in;

                mar <= mar + 1;
            end else if (tstate == 6) begin
                sp <= tmp << 8 | io_data_in;

                mar <= mar + 1;
            end else if (tstate == 7) begin
                x1 <= io_data_in;

                mar <= mar + 1;
            end else if (tstate == 8) begin
                x2 <= io_data_in;

                mar <= mar + 1;
            end else if (tstate == 9) begin
                x3 <= io_data_in;

                mar <= mar + 1;
            end else if (tstate == 10) begin
                x4 <= io_data_in;

                mar <= mar + 1;
            end else if (tstate == 11) begin
                x5 <= io_data_in;

                mar <= mar + 1;
            end else if (tstate == 12) begin
                x6 <= io_data_in;

                mar <= mar + 1;
            end else if (tstate == 13) begin
                x7 <= io_data_in;

                mar <= mar + 1;
            end else if (tstate == 14) begin
                x8 <= io_data_in;

                mar <= mar + 1;
            end else if (tstate == 15) begin
                single_step <= io_data_in;
            end else if (tstate == 16) begin
                tstate <= 0;
            end
        end else if (ir == `OP_HLT) begin
            halted <= 1;
        end
    end
end

endmodule
