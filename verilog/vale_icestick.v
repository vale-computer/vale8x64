/*
Copyright (C) 2018-2019 Francis Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

`default_nettype none

module vale_icestick(
    input clk,
    input RS232_Rx,
    output RS232_Tx,
    input DTRn,
    input RTSn,
    output LED1,
    output LED2,
    output LED3,
    output LED4,
    output LED5,
    output J1_3,
    output J1_4,
    output J1_5,
    output J1_6,
    output J1_7,
    output J1_8,
    output J1_9,
    output J1_10
);

`include "config.v"

wire rst;

// Serial I/O

wire ser_tx;
wire [7:0] ser_tx_data;
wire ser_rx;
wire [7:0] ser_rx_data;
wire ser_rx_valid;
wire ser_is_busy;

// Debug
wire [7:0] pc;
wire [7:0] ir;
wire [7:0] x1;
wire [7:0] x2;
wire [7:0] x3;
wire [7:0] x4;

buart buart(
    .clk(clk),
    .resetq(!rst),
    .rx(RS232_Rx),
    .tx(RS232_Tx),
    .rd(ser_rx),
    .wr(ser_tx),
    .valid(ser_rx_valid),
    .busy(ser_is_busy),
    .tx_data(ser_tx_data),
    .rx_data(ser_rx_data)
);

cpu cpu(
    .clk_master(clk),
    .clk(clk),
    .rst(rst),
    .ser_tx(ser_tx),
    .ser_tx_data(ser_tx_data),
    .ser_rx(ser_rx),
    .ser_rx_data(ser_rx_data),
    .ser_rx_valid(ser_rx_valid),
    .ser_is_busy(ser_is_busy),
    .dbg_pc_out(pc),
    .dbg_ir_out(ir),
    .dbg_x1_out(x1),
    .dbg_x2_out(x2),
    .dbg_x3_out(x3),
    .dbg_x4_out(x4)
);

endmodule
