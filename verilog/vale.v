/*
Copyright (C) 2018-2019 Francis Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

`default_nettype none

`include "interrupt.v"

`define SERIAL_RX 16'hf1
`define SERIAL_TX 16'hf2
`define SERIAL_RX_ACK 16'hf0
`define SERIAL_TX_DATA 16'hf3

`define SERIAL_RX_INT 16'h0013

module vale(
    input clk,
    input clr,
    output rs232_tx_out,
    input rs232_rx_in,
    output dbg_0,
    output dbg_1,
    output dbg_2,
    output dbg_3,
    output dbg_4,
    output dbg_5,
    output dbg_6,
    output dbg_7
);

`include "config.v"

/*
Serial I/O
*/

reg ser_tx;
reg [7:0] ser_tx_data;
wire [7:0] ser_rx_data;
wire ser_rx_valid;
wire ser_is_busy;

// Last character received
reg [7:0] rx_buf;
initial rx_buf = 0;

rs232_tx rs232_tx(
    .clk(clk),
    .tx(ser_tx),
    .tx_byte(ser_tx_data),
    .is_busy(ser_is_busy),
    .tx_port(rs232_tx_out)
);

rs232_rx rs232_rx(
    .clk(clk),
    .rx_byte(ser_rx_data),
    .is_valid(ser_rx_valid),
    .rx_port(rs232_rx_in)
);

/*
Program RAM
*/

reg prog_ram_load;
reg [7:0] data_to_prog_ram;
wire [7:0] prog_ram_out;

prog_ram prog_ram(
    .clk(clk),
    .load(prog_ram_load),
    .addr(addr[11:0]),
    .in(data_to_prog_ram),
    .out(prog_ram_out)
);

/*
CPU
*/

// I/O

wire [15:0] addr;
reg [7:0] data_to_cpu;
wire [7:0] data_from_cpu;
wire read;
wire load;

// Interrupts

wire [1:0] interrupt;
wire [7:0] interrupt_addr;

// Debug

wire [7:0] pc;
wire [7:0] ir;
wire [7:0] x1;
wire [7:0] x2;
wire [7:0] x3;
wire [7:0] x4;
wire [7:0] x5;
wire [7:0] x6;
wire [7:0] x7;
wire [7:0] x8;

cpu cpu(
    .clk(clk),
    .clr(clr),
    .io_addr(addr),
    .io_data_in(data_to_cpu),
    .io_data_out(data_from_cpu),
    .io_read(read),
    .io_load(load),
    .interrupt(interrupt),
    .interrupt_addr(interrupt_addr),
    .dbg_pc_out(pc),
    .dbg_ir_out(ir),
    .dbg_x1_out(x1),
    .dbg_x2_out(x2),
    .dbg_x3_out(x3),
    .dbg_x4_out(x4),
    .dbg_x5_out(x5),
    .dbg_x6_out(x6),
    .dbg_x7_out(x7),
    .dbg_x8_out(x8)
);

/*
I/O and address decoding logic
*/

assign interrupt = ser_rx_valid ? `INT_NEXT_INST : 0;
assign interrupt_addr = ser_rx_valid ? `SERIAL_RX_INT : 0;

always @* begin
    data_to_cpu <= data_to_cpu;
    data_to_prog_ram <= data_to_prog_ram;
    prog_ram_load <= 0;

    case (addr)
        `SERIAL_RX: begin
            data_to_cpu <= rx_buf;
        end
        `SERIAL_TX: begin
            data_to_cpu <= ser_is_busy;
        end
        default: begin
            data_to_cpu <= prog_ram_out;
            data_to_prog_ram <= data_from_cpu;

            prog_ram_load <= load;
        end
    endcase
end

always @(posedge clk) begin
    ser_tx_data <= ser_tx_data;
    ser_tx <= 0;

    rx_buf <= rx_buf;

    case (load)
        1: begin
            case (addr)
                `SERIAL_TX_DATA: begin
                    ser_tx_data <= data_from_cpu;
                    ser_tx <= 1;
                end
            endcase
        end
    endcase

    if (ser_rx_valid) begin
        rx_buf <= ser_rx_data;
    end

    if (addr == `SERIAL_RX && read) begin
        if (rx_buf) begin
            rx_buf <= 0;
        end
    end
end

endmodule
