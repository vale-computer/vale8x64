"""
Copyright (C) 2018-2019 Francis Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
"""

import json
import sys

inst_filename = sys.argv[1]

with open(inst_filename, "r") as f:
    instructions = json.loads(f.read())

with open("opcodes.v", "w") as f:
    for inst in instructions:
        name = inst["mnem"].upper()
        name = name.replace(",", "_")
        name = name.replace(" ", "_")
        name = name.replace("(", "_")
        name = name.replace(")", "_")
        name = "OP_" + name
        opcode = str.format("8'h{:02x}", inst["opcode"])
        f.write(str.format("`define {:<25}{}\n", name, opcode))

