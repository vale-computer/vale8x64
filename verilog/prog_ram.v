/*
Copyright (C) 2018-2019 Francis Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

module prog_ram(
  input clk,
  input load,
  input [11:0] addr,
  input [7:0] in,
  output [7:0] out
);

reg [7:0] ram [4095:0];

initial begin
  $readmemh("datafile_syn.hex", ram);
end

assign out = ram[addr];

always @(posedge clk) begin
  if (load) begin
    ram[addr] = in;
  end
end

endmodule
