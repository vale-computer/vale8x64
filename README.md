This is the repository for the VALE8x64 project.

Background
----------

The VALE8x64 project is an effort to create a modern, 8-bit programmable
computer powerful enough to inspire creativity and simple enough to be
completely understood by the motivated user.

These are the specifications and goals for the first version of the product:

* 16-bit address width for up to 64KiB of memory.
* 8-bit data width.
* Memory-mapped I/O.
* RS-232 serial port I/O.
* Character cell VGA graphics.
* A sound generator.
* A hosted machine code monitor for debugging.
* A non-hosted assembler for writing programs.

Project Status
--------------

VALE8x64 is in a very, very early stage of development. I'm relatively new to
digital logic, assembly language, assembler/compiler design, and FPGAs, and am
learning from prior work and through experimentation. As such, the repository
in its current state can be seen more as a playground for my ideas and
beginner's mistakes than as an intermediate step in a carefully planned design.

Preliminary versions of the RS-232 controller, instruction set, CPU/memory,
assembler, and machine code monitor are committed.

The character cell VGA display controller has been developed and tested, but is
not yet committed or integrated with the system.

An emulator written in C with SDL has been started, but is not yet committed.
It may be committed when the instruction set is more stable, but is not a
primary goal for the first version of the product.

Hardware Implementation
-----------------------

Earlier digital logic experiments were built on a breadboard using 74 series
logic ICs, with Ben Eater's YouTube tutorials and Digital Computer Electronics
(Malvino and Brown) at hand.

The current work targets the Lattice iCE40-UP5K on the ICE40UP5K-B-EVN breakout
board.

Development Plan
----------------

I plan to work on VALE8x64 full-time for at least the remainder of 2019.

Here are a few upcoming milestones, from highest to lowest priority:

* Finish the machine code monitor. The feature set is loosely based on that of the
  [VIC Machine Code Monitor](https://archive.org/details/VICMachineCodeMonitor).
* Develop the assembly language routines for line-oriented display drawing.
* Finish the VGA controller and integrate it with the system.
* Specify and start work on the sound generator.

Updates
-------

I add frequent development updates on [Twitter](https://twitter.com/fmahnke)
(meh) and on [Mastodon](https://fosstodon.org/@daremo) (preferred).
