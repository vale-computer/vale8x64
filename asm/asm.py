"""
Copyright (C) 2018-2019 Francis Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
"""

from enum import Enum, auto
import json
import logging
import pprint
import sys

class TokenType(Enum):
    INSTR = auto()
    DIREC = auto()
    LABEL = auto()
    NUMERIC = auto()
    OPERATOR = auto()

class Operator(Enum):
    ADD = auto()

class Lex(Enum):
    NONE = auto()
    UNNAMED_REVERSE_LABEL = auto()
    UNNAMED_FORWARD_LABEL = auto()
    STRING = auto()
    IDENTIFIER = auto()

class Status(Enum):
    OK = auto()
    ERR = auto()
    ERR_BOUNDS = auto()

def hi_lo(addr):
    """ Separate and return the high and low bytes of a 16-bit integer. """
    hi = addr >> 8;
    lo = addr & 0xff;

    return (hi, lo)

def is_number(val):
    if val[0] == "$":
        return True
    else:
        return False

def is_str(val):
    if val[0] == "\"" and val[-1] == "\"":
        return True
    else:
        return False

def hex_str_to_int(hex_str):
    hex_str = hex_str.strip()

    try:
        return int(hex_str[1:], 16)
    except ValueError:
        return None

def string_parse(string):
    return string[1:-1]

def bytes_(tokens):
    bytes_ = []

    for token in tokens:
        if is_number(token):
            bytes_.append(hex_str_to_int(token))
        elif is_str(token):
            str_ = string_parse(token)
            for char in str_:
                bytes_.append(ord(char))

    return bytes_

def instructions_load():
    instructions = []

    with open("instructions.json") as f:
        inst_json = json.loads(f.read())
    with open("pseudoinstructions.json") as f:
        pseudo_json = json.loads(f.read())

    inst_json = inst_json + pseudo_json

    for entry in inst_json:
        tokens = entry["mnem"].split()
        mnem = entry["mnem"]
        name = tokens[0]

        if "opcode" in entry:
            opcode = entry["opcode"]
        else:
            opcode = None

        size = entry["size"]

        if len(tokens) > 1:
            operands = tokens[1].split(",")

        if name in ["nop", "brk", "res"]:
            instructions.append({"token_type": TokenType.INSTR, "size": size,
                "mnem": mnem, "name": name, "opcode": opcode})
        elif name == "li":
            dest = operands[0]
            instructions.append({"token_type": TokenType.INSTR, "size": size,
                "mnem": mnem, "name": name, "dest": dest, "opcode": opcode})
        elif name in ["jmp", "call"]:
            base = operands[0]
            instructions.append({"token_type": TokenType.INSTR, "size": size,
                "mnem": mnem, "name": name, "base": base, "opcode": opcode})
        elif name == "ret":
            instructions.append({"token_type": TokenType.INSTR, "size": size,
                "mnem": mnem, "name": name, "opcode": opcode})
        elif name in ["beq", "bne", "blt"]:
            instructions.append({"token_type": TokenType.INSTR, "size": size,
                "mnem": mnem, "opcode": opcode, "name": name, "src1": operands[0],
                "src2": operands[1]})
        elif name == "lb":
            dest = operands[0]

            base_start = operands[1].find("(")
            base_end = operands[1].find(")")
            base = operands[1][base_start + 1:base_end]

            instructions.append({"token_type": TokenType.INSTR, "size": size,
                "mnem": mnem, "opcode": opcode, "name": name, "dest": dest,
                "base": base})
        elif name == "sb":
            dest = operands[0]

            base_start = operands[1].find("(")
            base_end = operands[1].find(")")
            base = operands[1][base_start + 1:base_end]

            instructions.append({"token_type": TokenType.INSTR, "size": size,
                "mnem": mnem, "opcode": opcode, "name": name, "dest": dest,
                "base": base})
        elif name in ["mov", "add", "sub", "and", "srl"]:
            instructions.append({"token_type": TokenType.INSTR, "size": size,
                "mnem": mnem, "opcode": opcode, "name": name, "dest": operands[0],
                "src": operands[1]})
        elif name in ["sll", "orr"]:
            instructions.append({"token_type": TokenType.INSTR, "size": size,
                "mnem": mnem, "opcode": opcode, "name": name,
                "dest": operands[0], "src1": operands[1], "src2": operands[2]})

    return instructions

def preprocess(in_filename, inc_dir, included):
    logging.info("Include begin " + in_filename)

    processed = []
    defns = []

    in_file = open(in_filename, "r")

    inside_macro = False

    while True:
        line = in_file.readline()

        logging.info("Line: " + line)

        if line == "":
            # Break at end of file.
            break

        if line == "\n":
            # Skip blank line.
            continue

        line = line.strip()
        tokens = line.split()

        if not inside_macro:
            if tokens[0] == ".include":
                include_filename = string_parse(tokens[1])

                if include_filename not in included:
                    (sub_defns, sub_processed) = preprocess(inc_dir + "/" + include_filename,
                        inc_dir, included)
                    processed += sub_processed
                    defns += sub_defns
                    included.append(include_filename)
                else:
                    logging.info(str.format("Include skip; {} already included",
                        include_filename));
            elif tokens[0] == ".define":
                defns.append({"name": tokens[1], "val": tokens[2]})
            elif tokens[0] == ".macro":
                defns.append({"name": tokens[1], "args": tokens[2:], "val": []})
                inside_macro = True
                macro = defns[-1]
            else:
                processed.append(line)
        else:
            if tokens[0] == ".endm":
                inside_macro = False
            else:
                macro["val"].append(line)

    in_file.close()

    logging.info("Include complete for " + in_filename)
    return (defns, processed)

def tokenize(line):
    tokens = []
    token = ""

    lex_type = Lex.NONE

    for i in range(0, len(line)):
        logging.debug("Lex " + str(lex_type) + " char " + line[i])

        if line[i] == ";":
            if lex_type == Lex.NONE:
                break
            else:
                raise Exception()
        elif line[i] == "\"":
            if lex_type == Lex.NONE:
                lex_type = Lex.STRING
                token += line[i]
            elif lex_type == Lex.STRING:
                token += line[i]
                tokens.append(token)
                token = ""
                lex_type = Lex.NONE
            else:
                raise Exception()
        elif line[i] == "(":
            if lex_type != Lex.NONE:
                tokens.append(token)
                token = ""
                lex_type = Lex.NONE
                tokens.append(line[i])
            else:
                raise Exception()
        elif line[i] == ")":
            if lex_type != Lex.NONE:
                tokens.append(token)
                token = ""
                lex_type = Lex.NONE
                tokens.append(line[i])
            else:
                raise Exception()
        elif line[i] == "-":
            if lex_type == Lex.NONE:
                lex_type = Lex.UNNAMED_REVERSE_LABEL
                token += line[i]
            elif lex_type == Lex.UNNAMED_REVERSE_LABEL:
                token += line[i]
            else:
                raise Exception()
        elif line[i] == "+":
            if lex_type == Lex.NONE:
                lex_type = Lex.UNNAMED_FORWARD_LABEL
                token += line[i]
            elif lex_type == Lex.UNNAMED_FORWARD_LABEL:
                token += line[i]
            elif lex_type == Lex.IDENTIFIER:
                tokens.append(token)
                tokens.append(line[i])
                token = ""
                lex_type = Lex.NONE
            else:
                raise Exception()
        elif line[i] == " ":
            if lex_type in [Lex.UNNAMED_REVERSE_LABEL,
                Lex.UNNAMED_FORWARD_LABEL, Lex.IDENTIFIER]:

                tokens.append(token)
                token = ""
                lex_type = Lex.NONE
            elif lex_type == Lex.STRING:
                token += line[i]
            else:
                raise Exception()
        elif line[i] == ",":
            if lex_type == Lex.IDENTIFIER:
                tokens.append(token)
                tokens.append(line[i])
                token = ""
                lex_type = Lex.NONE
        else:
            if lex_type == Lex.NONE:
                lex_type = Lex.IDENTIFIER

            token += line[i]

    if token != "":
        tokens.append(token)

    return tokens

def macro_expand(defns, input_data):
    macros_expanded = []
    defns_replaced = []

    # Replace in order of names from longest to shortest. This ensures shorter
    # names that are part of longer ones aren't expanded first, which would 
    # lead to longer ones being expanded incorrectly.

    defns = sorted(defns, key=lambda l: len(l["name"]), reverse=True)

    # Expand macros before expanding defines. This ensures defines used
    # within macros are properly expanded.

    # Expand macros.

    for line in input_data:
        expansion = []
        for d in defns:
            tokens = line.split()

            if tokens[0] == d["name"] and type(d["val"]) == list:
                # This is a macro.
                args_vals = []

                # Build a map of positional argument names to their values
                # in this macro invocation.
                for i in range(0, len(d["args"])):
                    args_vals.append({"name": d["args"][i],
                        "val": tokens[1 + i]})

                logging.info("Macro arguments")
                logging.info(args_vals)

                # For each statement in the macro, replace the name of any
                # argument with its value.
                for st in d["val"]:
                    args_replaced = st
                    for arg in args_vals:
                        args_replaced = args_replaced.replace(arg["name"], arg["val"])

                    expansion.append(args_replaced)

        # If this line was a macro, add the expansion. Otherwise, add the
        # source line with no changes.
        if expansion:
            macros_expanded += expansion
        else:
            macros_expanded.append(line)

    # Expand defines.

    for line in macros_expanded:
        for d in defns:
            if type(d["val"]) == str:
                # This is a define.
                line = line.replace(d["name"], d["val"])
        defns_replaced.append(line)

    return defns_replaced

def node_create(imm):
    """ Create a syntax tree node. """

    if "token_type" in imm:
        node = imm
    elif type(imm[0]) is int:
        node = {"token_type": TokenType.NUMERIC, "val": imm[0]}
    elif imm[0] == "+":
        node = {"token_type": TokenType.LABEL, "name": imm[0]}
    elif "+" in imm:
        node = {"token_type": TokenType.OPERATOR, "operator": Operator.ADD,
            "operands": [node_create([imm[0]]), node_create([imm[2]])]}
    elif is_number(imm[0]):
        node = {"token_type": TokenType.NUMERIC, "val": hex_str_to_int(imm[0])}
    else:
        node = {"token_type": TokenType.LABEL, "name": imm[0]}

    return node

def ast_build(input):
    ast = []

    for entry in input:
        logging.debug("AST node " + str(entry))

        if "mnem" not in entry:
            ast.append(entry)
        elif entry["mnem"] in ["nop", "ret", "brk", "res"]:
            inst = list(filter(lambda l: l["name"] == entry["mnem"],
                instructions))[0].copy()
            ast.append({**entry, **inst})
        elif entry["mnem"] == "li":
            inst = list(filter(lambda l: l["name"] == entry["mnem"] and
                l["dest"] == entry["dest"], instructions))[0].copy()
            node = {**entry, **inst}
            node["imm"] = node_create(node["imm"])
            ast.append(node)
        elif entry["mnem"] in ["jmp", "call"]:
            inst = list(filter(lambda l: l["name"] == entry["mnem"] and
                l["base"] == entry["base"], instructions))[0].copy()
            node = {**entry, **inst}
            node["offset"] = node_create(node["offset"])
            ast.append(node)
        elif entry["mnem"] in ["beq", "bne", "blt"]:
            inst = list(filter(lambda l: l["name"] == entry["mnem"] and
                l["src1"] == entry["src1"] and l["src2"] == entry["src2"],
                instructions))[0].copy()
            node = {**entry, **inst}
            node["imm"] = node_create(node["imm"])
            node["offset"] = node_create(node["offset"])
            ast.append(node)
        elif entry["mnem"] in ["lb", "sb"]:
            inst = list(filter(lambda l: l["name"] == entry["mnem"] and
                l["base"] == entry["base"] and l["dest"] == entry["dest"],
                instructions))[0].copy()
            node = {**entry, **inst}
            node["offset"] = node_create(node["offset"])
            ast.append(node)
        elif entry["mnem"] in ["mov", "add", "sub", "and", "srl"]:
            inst = list(filter(lambda l: l["name"] == entry["mnem"] and
                l["dest"] == entry["dest"] and l["src"] == entry["src"],
                instructions))[0].copy()
            node = {**entry, **inst}
            node["imm"] = node_create(node["imm"])
            ast.append(node)
        elif entry["mnem"] in ["sll", "orr"]:
            inst = list(filter(lambda l: l["name"] == entry["mnem"] and
                l["dest"] == entry["dest"] and l["src1"] == entry["src1"] and
                l["src2"] == entry["src2"], instructions))[0].copy()
            ast.append({**entry, **inst})

    return {"result": ast}

def lex_build(input):
    lex = []
    status = Status.OK

    for line in input:
        logging.info("Line: " + line)

        tokens = tokenize(line)

        logging.info("Tokenized line: " + str(tokens))

        if len(tokens) == 0:
            continue
        elif tokens[0].endswith(":"):
            # Named label
            lex.append({"token_type": TokenType.LABEL, "name": tokens[0][:-1]})

            tokens = tokens[1:]
        elif tokens[0][0] in ["-", "+"]:
            # Unnamed label
            lex.append({"token_type": TokenType.LABEL, "name": tokens[0]})

            tokens = tokens[1:]

        if len(tokens) == 0:
            continue

        name = tokens[0].lower()

        if name in ["nop", "ret", "brk", "res"]:
            lex.append({"mnem": name, "source": str.format("{}", name)})
        elif name == "li":
            dest = tokens[1]
            imm = tokens[3:]

            lex.append({"mnem": name, "dest": dest, "imm": imm,
                "source": str.format("{} {},{}", name, dest, "".join(imm))})
        elif name in ["jmp", "call"]:
            base = tokens[1]
            offset = tokens[3:]

            lex.append({"mnem": name, "base": base, "offset": offset,
                "source": str.format("{} {},{}", name, base, "".join(offset))})
        elif name in ["beq", "bne", "blt"]:
            src1 = tokens[1]
            src2 = tokens[3]
            imm_end_idx = tokens.index(",", 5)
            imm = tokens[5:imm_end_idx]
            offset = tokens[imm_end_idx + 1:]

            lex.append({"mnem": name, "src1": src1, "src2": src2,
                "imm": imm, "offset": offset,
                "source": str.format("{} {},{},{},{}", name,
                src1, src2, "".join(imm), "".join(offset))})
        elif name in ["lb", "sb"]:
            base_idx = tokens.index("(") + 1
            dest = tokens[1]
            offset = tokens[3:base_idx - 1]
            base = tokens[base_idx]

            lex.append({"mnem": name, "base": base, "dest": dest,
                "offset": offset, "source": str.format("{} {},{}({})", name,
                dest, "".join(offset), base)})
        elif name in ["mov", "add", "sub", "and", "srl"]:
            dest = tokens[1]
            src = tokens[3]
            imm = tokens[5:]

            lex.append({"mnem": name, "dest": dest, "src": src, "imm": imm,
                "source": str.format("{} {},{},{}", name, dest, src,
                "".join(imm))})
        elif name in ["sll", "orr"]:
            dest = tokens[1]
            src1 = tokens[3]
            src2 = tokens[5]

            lex.append({"mnem": name, "dest": dest, "src1": src1, "src2": src2,
                "source": str.format("{} {},{},{}", name, dest, src1, src2)})
        elif name == ".org":
            entry = {"token_type": TokenType.DIREC, "name": name,
                "offset": tokens[1]}
            lex.append(entry)
        elif name == ".byte":
            data = bytes_(tokens[1:])
            entry = {"token_type": TokenType.DIREC, "name": name, "data": data,
                "size": len(data)}
            lex.append(entry)
        else:
            logging.error("Skipping invalid instruction name: " + name)
            status = Status.ERR

    return {"status": status, "result": lex}

def pseudo_expand(input_data):
    output = []

    for i in input_data:
        if "mnem" in i and i["name"] == "li" and len(i["dest"]) > 2:
            logging.debug("Pseudo expansion " + str(i))

            # 16-bit load

            dest = i["dest"]
            imm = i["imm"]

            dest_0 = dest[0:2]
            dest_1 = dest[0] + dest[2:]

            (hi, lo) = hi_lo(imm["val"])

            inst = list(filter(lambda l: l["name"] == "li" and
                l["dest"] == dest_0, instructions))[0].copy()

            inst["dest"] = dest_0
            inst["imm"] = node_create([hi])
            inst["source"] = i["source"]
            inst["pos"] = i["pos"]
            output.append(inst)

            inst = list(filter(lambda l: l["name"] == "li" and
                l["dest"] == dest_1, instructions))[0].copy()

            inst["dest"] = dest_1
            inst["imm"] = node_create([lo])
            inst["source"] = i["source"]
            inst["pos"] = i["pos"] + 2
            output.append(inst)
        else:
            output.append(i)

    return {"result": output}

def position(ast):
    """ Position the code. """
    pos = 0

    labels = []

    for i in range(0, len(ast)):
        entry = ast[i]
        if entry["token_type"] == TokenType.INSTR:
            entry["pos"] = pos
            pos += entry["size"]
        elif entry["token_type"] == TokenType.DIREC:
            if entry["name"] == ".org":
                pos = hex_str_to_int(entry["offset"])
            if entry["name"] == ".byte":
                entry["pos"] = pos
                pos += entry["size"]
        elif entry["token_type"] == TokenType.LABEL:
            labels.append({"name": entry["name"], "pos": pos})

    return (ast, labels)

def locations_resolve(code, labels):
    def label_resolve(node, pos, rel=False):
        logging.info("Resolve label " + str(node))
        # Choose the right label

        cand = list(filter(lambda l: l["name"] == node["name"], labels))
        logging.debug("Candidates " + str(cand))
        distances = []
        for i in range(0, len(cand)):
            distances.append({"label": cand[i], "dist": cand[i]["pos"] - pos})
        distances = sorted(distances, key=lambda l: l["dist"])
        logging.debug("Distances " + str(distances))

        if node["name"][0] == "+":
            label = list(filter(lambda l: l["dist"] > 0,
                distances))[0]["label"]
        elif node["name"][0] == "-":
            label = list(filter(lambda l: l["dist"] < 0,
                distances))[-1]["label"]
        else:
            label = list(filter(lambda l: l["name"] == node["name"],
                labels))[0]

        logging.debug("Resolved to label " + str(label))

        if rel:
            loc = label["pos"] - pos

            if loc < 0:
                loc = 0x100 + loc
        else:
            loc = label["pos"]

        return node_create([loc])

    def pos_resolve(operand):
        if operand["token_type"] == TokenType.LABEL:
            return label_resolve(operand, entry["pos"], rel=rel)
        elif operand["token_type"] == TokenType.NUMERIC:
            return operand
        else:
            raise Exception()

    def operand_resolve(operand):
        if operand["token_type"] == TokenType.OPERATOR:
            operand["operands"] = [pos_resolve(op) for op in operand["operands"]]

            return operand
        else:
            return pos_resolve(operand)

    for i in range(0, len(code)):
        entry = code[i]

        logging.debug("Resolve entry " + str(entry))

        if entry["token_type"] == TokenType.INSTR:
            if entry["name"] in ["beq", "bne", "blt"]:
                rel = True
            else:
                rel = False

            # Resolve each of the operands in the instruction.

            if "imm" in entry:
                entry["imm"] = operand_resolve(entry["imm"])

            if "offset" in entry:
                entry["offset"] = operand_resolve(entry["offset"])

    return code

def operations(input_data):
    def ops_apply(node):
        if "operator" in node:
            if node["operator"] == Operator.ADD:
                return node_create([node["operands"][0]["val"] +
                    node["operands"][1]["val"]])
            else:
                raise Exception()
        else:
            return node

    for i in range(0, len(input_data)):
        entry = code[i]

        logging.debug("Operations on entry " + str(entry))

        if entry["token_type"] == TokenType.INSTR:
            if "imm" in entry:
                entry["imm"] = ops_apply(entry["imm"])
            if "offset" in entry:
                entry["offset"] = ops_apply(entry["offset"])

    return input_data

def int8_validate(val):
    if val < 0x0 or val > 0xff:
        return False
    else:
        return True

def int16_validate(val):
    if val < 0x0 or val > 0xffff:
        return False
    else:
        return True

def assemble_instr(instr):
    status = Status.OK

    byte_list = []

    byte_list.append(instr["opcode"])

    if instr["name"] in ["nop", "ret", "sll", "orr", "brk", "res"]:
        pass
    elif instr["name"] in ["jmp", "call"]:
        if not int16_validate(instr["offset"]["val"]):
            status = Status.ERR_BOUNDS

        (hi, lo) = hi_lo(instr["offset"]["val"])
        byte_list.append(lo)
        byte_list.append(hi)
    elif instr["name"] in ["beq", "bne", "blt"]:
        if not int8_validate(instr["imm"]["val"]) or \
            not int8_validate(instr["offset"]["val"]):

            status = Status.ERR_BOUNDS

        byte_list.append(instr["imm"]["val"])
        byte_list.append(instr["offset"]["val"])
    elif instr["name"] in ["li", "mov", "add", "sub", "and", "srl"]:
        if not int8_validate(instr["imm"]["val"]):
            status = Status.ERR_BOUNDS

        byte_list.append(instr["imm"]["val"])
    elif instr["name"] in ["lb", "sb"]:
        if not int8_validate(instr["offset"]["val"]):
            status = Status.ERR_BOUNDS

        byte_list.append(instr["offset"]["val"])
    else:
        raise Exception("Unknown instruction: " + instr["name"])

    return {"status": status, "result": byte_list}

def assemble(code, labels):
    status = Status.OK

    pos = 0
    byte_list = []

    for entry in code:
        label_name = ""

        # If this is a positioned entry, search for a label at the
        # position. If one exists, store its name so it can be added
        # to the assembly log.

        if "pos" in entry:
            for label in labels:
                if label["pos"] == entry["pos"]:
                    label_name = label["name"]

                    if label_name[0] != "+" and label_name[0] != "-":
                        label_name += ":"

        if entry["token_type"] == TokenType.INSTR:
            if entry["pos"] > pos:
                diff = entry["pos"] - pos

                for i in range(0, diff):
                    info_str = str.format("{:04x} {:02x}", pos, 0)
                    logging.info(info_str)
                    byte_list.append(0)
                    pos += 1

            logging.debug("Assemble: " + str(entry))

            ass = assemble_instr(entry)

            if ass["status"] != Status.OK:
                status = ass["status"]
                logging.error(str.format("Assemble error: {} {}", status, str(entry)))

                break

            logging.info(str.format("{:04x} {:02x} {} {}", pos, ass["result"][0],
                label_name, entry["mnem"]))
            byte_list.append(ass["result"][0])
            pos += 1
            for b in ass["result"][1:]:
                logging.info(str.format("{:04x} {:02x}", pos, b))
                byte_list.append(b)
                pos += 1
        elif entry["token_type"] == TokenType.DIREC:
            if entry["name"] == ".byte":
                if entry["pos"] > pos:
                    diff = entry["pos"] - pos

                    for i in range(0, diff):
                        info_str = str.format("{:04x} {:02x}", pos, 0)
                        logging.info(info_str)
                        byte_list.append(0)
                        pos += 1

                for b in entry["data"]:
                    info_str = str.format("{:04x} {:02x} {}", pos, b, label_name)
                    logging.info(info_str)

                    # Print the label name only before the first data byte,
                    # not before each of them.

                    if label_name != "":
                        label_name = ""

                    byte_list.append(b)
                    pos += 1

    return {"status": status, "result": byte_list}

def out_fmt(code):
    return list(map(lambda l: str.format("{:02x}", l), code))

def args_process(args):
    loglevel = logging.WARNING
    inc_dir = ""

    for i in range(0, len(args)):
        arg = args[i]

        if arg == "-i":
            inc_dir = args[i + 1]
            i += 1
        elif arg == "-v":
            loglevel = logging.INFO
        elif arg == "-vv":
            loglevel = logging.DEBUG
        else:
            in_filename = arg

    return {"inc_dir": inc_dir, "in_filename": in_filename,
        "loglevel": loglevel}

if __name__ == "__main__":
    exit_code = 0
    args = sys.argv[1:]

    config = args_process(args)

    logging.basicConfig(level=config["loglevel"])

    logging.info("Input filename: " + config["in_filename"])

    instructions = instructions_load()

    logging.info("Instructions start")
    logging.info(pprint.pformat(instructions))
    logging.info("Instructions finish")

    logging.info("Preprocess start")
    included = []
    (defns, intermediate) = preprocess(config["in_filename"], config["inc_dir"],
        included)
    logging.info("Preprocess result")

    logging.info("Defines")
    logging.info(pprint.pformat(defns))
    logging.info("Intermediate")
    logging.info(pprint.pformat(intermediate))

    logging.info("Macro expansion start")
    intermediate = macro_expand(defns, intermediate)
    logging.info("Macro expansion result")
    logging.info(pprint.pformat(intermediate))

    logging.info("Lex start")
    intermediate = lex_build(intermediate)

    if intermediate["status"] != Status.OK:
        exit_code = intermediate["status"]

    logging.info("Lex result")
    logging.info(pprint.pformat(intermediate["result"]))

    logging.info("AST start")
    intermediate = ast_build(intermediate["result"])
    logging.info("AST result")

    logging.info(pprint.pformat(intermediate["result"]))

    logging.info("Position start")
    (code, labels) = position(intermediate["result"])

    logging.info("Position finish")

    logging.info("Labels")
    logging.info(pprint.pformat(labels))

    logging.info("Location resolve start")
    code = locations_resolve(code, labels)
    logging.info("Location resolve result")
    logging.info(pprint.pformat(code))

    logging.info("Operations start")
    code = operations(code)
    logging.info("Operations result")
    logging.info(pprint.pformat(code))

    logging.info("Pseudo expansion start")
    code = pseudo_expand(code)
    logging.info("Pseudo expansion result")
    logging.info(pprint.pformat(code["result"]))

    logging.info("Assemble start")
    code = assemble(code["result"], labels)

    if code["status"] != Status.OK:
        exit_code = code["status"]

    logging.info("Assemble finish")

    output = out_fmt(code["result"])

    logging.info("Output")
    logging.info(pprint.pformat(output))

    for line in output:
        print(line)

    sys.exit(exit_code)

