"""
Copyright (C) 2018-2019 Francis Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
"""

import json

instructions = [
    {"mnem": "nop", "size": 1},
    {"mnem": "li x1,imm", "size": 2},
    {"mnem": "li x2,imm", "size": 2},
    {"mnem": "li x3,imm", "size": 2},
    {"mnem": "li x4,imm", "size": 2},
    {"mnem": "li x5,imm", "size": 2},
    {"mnem": "li x6,imm", "size": 2},
    {"mnem": "li x7,imm", "size": 2},
    {"mnem": "li x8,imm", "size": 2},
    {"mnem": "jmp x00,offset", "size": 3},
    {"mnem": "jmp x34,offset", "size": 3},
    {"mnem": "call x00,offset", "size": 3},
    {"mnem": "call x34,offset", "size": 3},
    {"mnem": "ret", "size": 1},
    {"mnem": "beq x1,x0,imm,offset", "size": 3},
    {"mnem": "beq x1,x2,imm,offset", "size": 3},
    {"mnem": "bne x1,x0,imm,offset", "size": 3},
    {"mnem": "bne x1,x2,imm,offset", "size": 3},
    {"mnem": "blt x1,x0,imm,offset", "size": 3},
    {"mnem": "blt x1,x2,imm,offset", "size": 3},
    {"mnem": "lb x1,offset(x34)", "size": 2},
    {"mnem": "lb x2,offset(x34)", "size": 2},
    {"mnem": "lb x1,offset(x56)", "size": 2},
    {"mnem": "lb x2,offset(x56)", "size": 2},
    {"mnem": "lb x1,offset(x78)", "size": 2},
    {"mnem": "lb x2,offset(x78)", "size": 2},
    {"mnem": "lb x1,offset(sp)", "size": 2},
    {"mnem": "lb x2,offset(sp)", "size": 2},
    {"mnem": "sb x1,offset(x34)", "size": 2},
    {"mnem": "sb x2,offset(x34)", "size": 2},
    {"mnem": "sb x1,offset(x56)", "size": 2},
    {"mnem": "sb x2,offset(x56)", "size": 2},
    {"mnem": "sb x1,offset(x78)", "size": 2},
    {"mnem": "sb x2,offset(x78)", "size": 2},
    {"mnem": "sb x1,offset(sp)", "size": 2},
    {"mnem": "sb x2,offset(sp)", "size": 2},
    {"mnem": "mov x1,x0,imm", "size": 2},
    {"mnem": "mov x1,x1,imm", "size": 2},
    {"mnem": "mov x1,x2,imm", "size": 2},
    {"mnem": "mov x1,x3,imm", "size": 2},
    {"mnem": "mov x1,x4,imm", "size": 2},
    {"mnem": "mov x1,x5,imm", "size": 2},
    {"mnem": "mov x1,x6,imm", "size": 2},
    {"mnem": "mov x1,x7,imm", "size": 2},
    {"mnem": "mov x1,x8,imm", "size": 2},
    {"mnem": "mov x2,x1,imm", "size": 2},
    {"mnem": "mov x2,x4,imm", "size": 2},
    {"mnem": "mov x2,x5,imm", "size": 2},
    {"mnem": "mov x3,x1,imm", "size": 2},
    {"mnem": "mov x4,x1,imm", "size": 2},
    {"mnem": "mov x5,x1,imm", "size": 2},
    {"mnem": "mov x6,x1,imm", "size": 2},
    {"mnem": "mov x7,x1,imm", "size": 2},
    {"mnem": "mov x8,x1,imm", "size": 2},
    {"mnem": "add x1,x0,imm", "size": 2},
    {"mnem": "add x1,x2,imm", "size": 2},
    {"mnem": "add x2,x0,imm", "size": 2},
    {"mnem": "add x5,x0,imm", "size": 2},
    {"mnem": "add x6,x0,imm", "size": 2},
    {"mnem": "add x34,x0,imm", "size": 2},
    {"mnem": "add x78,x0,imm", "size": 2},
    {"mnem": "add sp,x0,imm", "size": 2},
    {"mnem": "sub sp,x0,imm", "size": 2},
    {"mnem": "sll x1,x1,x2", "size": 1},
    {"mnem": "orr x1,x1,x2", "size": 1},
    {"mnem": "and x1,x0,imm", "size": 2},
    {"mnem": "srl x1,x0,imm", "size": 2},
    {"mnem": "sra", "size": 1},
    {"mnem": "brk", "size": 1},
    {"mnem": "res", "size": 1},
    {"mnem": "hlt", "size": 1},
]

pseudo = [
    {"mnem": "li x12,imm", "size": 4},
    {"mnem": "li x34,imm", "size": 4},
    {"mnem": "li x56,imm", "size": 4},
    {"mnem": "li x78,imm", "size": 4},
]

opcode = 0

for inst in instructions:
    inst["opcode"] = opcode
    opcode += 1

f = open("instructions.json", "w")
f.write(json.dumps(instructions, indent=1))

f.close()

f = open("instructions.txt", "w")

f.write(str.format("{:10s}\t{}\n", "Mnemonic", "Opcode"))
f.write(str.format("{}\t{}\n", "--------", "------"))

for inst in instructions:
    f.write(str.format("{:10s}\t{:08b}\t{:02x}\n",
        inst["mnem"], inst["opcode"],
        inst["opcode"]))

f.close()

f = open("pseudoinstructions.json", "w")
f.write(json.dumps(pseudo, indent=1))
f.close()
